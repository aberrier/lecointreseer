#!/usr/bin/env python3
from scrapy.http import HtmlResponse
import requests
import json
from environs import Env

if __name__ == "__main__":

    def join(array, delimiter=""):
        string = ""
        for index, elem in enumerate(array):
            string += str(elem)
            if index < len(array) - 1:
                string += delimiter
        return string

    env = Env()
    env.read_env()

    zapier_url = env("ZAPIER_HOOK_URL")

    XPATHS = {
        "appetizer": '//div[@class="pos17"]//td/text() | //div[@class="pos19"]//td/text()',
        "today_dish": '//div[@class="pos25"]//td/text() | //div[@class="pos27"]//td/text()',
        "sides": '//div[@class="pos33"]//td/text() | //div[@class="pos35"]//td/text()',
        "chef_dish": '//div[@class="pos41"]//td/text()',
        "desserts": '//div[@class="pos53"]//td/text() | //div[@class="pos55"]//td/text()',
        "no_data": '//div[@class="pos11"]/table[contains(@style, "hidden")]',
    }

    url = "https://7-11h.lecointreparis.com/Lp"
    r = requests.get(url)
    response = HtmlResponse(url, body=r.content)
    if not response.xpath(XPATHS["no_data"]).getall():
        appetizers = response.xpath(XPATHS["appetizer"]).getall()
        today_dish = response.xpath(XPATHS["today_dish"]).getall()
        chef_dish = response.xpath(XPATHS["chef_dish"]).getall()
        sides = response.xpath(XPATHS["sides"]).getall()
        desserts = response.xpath(XPATHS["desserts"]).getall()

    else:
        appetizers = ["Foie gras et tranches de sanglier du Pays Basque"]
        today_dish = ["Pain chilien à la viande"]
        chef_dish = ["Tamiyas, ful medames et houmous égyptien"]
        sides = ["Riz préférée de Remi", "Nouilles préférées de Romain"]
        desserts = ["Kouign amann au miel de Sologne", "Gateaux d'Ella"]
    data = {
        "appetizers": join(appetizers, " *|* "),
        "today_dish": join(today_dish, " *|* "),
        "chef_dish": join(chef_dish, " *|* "),
        "sides": join(sides, " *|* "),
        "desserts": join(desserts, " *|* "),
    }
    requests.post(zapier_url, json=json.dumps(data))
